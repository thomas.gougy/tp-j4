import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm'
import { Reservation } from './reservation.entity'
import { User } from './user.entity'

@Entity()
export class Order {

    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column({
        type: 'float'
    })
    price: number

    @Column({
        array: true
    })
    content: string

    @Column({
        type: 'boolean',
        default: false
    })
    validated: boolean;

    @ManyToOne(()=> User, (user)=> user.orders)
    user: User

    @ManyToOne(()=> Reservation, (reservation)=> reservation.orders)
    reservation: Reservation
}