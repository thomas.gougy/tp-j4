import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm'
import { Order } from './order.entity';
import { User } from './user.entity'


@Entity()
export class Reservation {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('boolean')
    validated: boolean;

    @Column('varchar')
    room: string;

    @ManyToOne(() => User, (user) => user.reservations)
    user: User

    @OneToMany(()=> Order, (order)=> order.reservation)
    orders: Order[]
} 
