import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'
import { Exclude } from 'class-transformer'
import { Order } from './order.entity'
import { Reservation } from './reservation.entity'

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string

    @Column('varchar', { unique: true })
    email: string

    @Exclude()
    @Column('varchar')
    password: string

    @Column('varchar')
    firstName: string
    
    @Column('varchar')
    lastName: string 

    @OneToMany(()=> Order, (order)=> order.user)
    orders: Order[]

    @OneToMany(()=> Reservation, (reservation)=> reservation.user)
    reservations: Reservation[]
}