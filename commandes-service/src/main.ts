import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { RedisService } from '@mistercoookie/nestjs-redis-pub-sub';

RedisService.initService('redis://51.83.68.39:6379')

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3003);
}
bootstrap();
