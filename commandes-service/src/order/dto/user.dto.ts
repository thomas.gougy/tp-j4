export class UserDTO {
    id: string
    lastName: string
    firstName: string
    email: string
}

interface RequestWithUser extends Request {
    user: UserDTO
}
export default RequestWithUser