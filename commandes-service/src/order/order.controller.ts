import { Body, Controller, Get, Param, Post, Put, Req, UseGuards } from "@nestjs/common";
import { ServiceAuthGuard } from "src/guard/auth.guard";
import { OrderDTO } from "./dto/order.dto";
import RequestWithUser from "./dto/user.dto";
import { OrderService } from "./order.service";
import { RedisService, RedisPublication } from "@mistercoookie/nestjs-redis-pub-sub";

@Controller('order')
export class OrderController {

    constructor(
        private orderService: OrderService
    ) {
        RedisService.subscribeChannel('ttjn-confirm-order', (redisPublication: RedisPublication<any>) => {
            this.validateOrder(redisPublication)
        })
     }

    @UseGuards(ServiceAuthGuard)
    @Post('/reservation/:idReservation')
    sendOrder(@Req() request: RequestWithUser, @Body() data: OrderDTO, @Param('idReservation') idReservation: string ) {
        return this.orderService.sendOneOrder(request.user, data, idReservation)
    }

    @UseGuards(ServiceAuthGuard)
    @Get()
    getMyOrder(@Req() request: RequestWithUser) {
        return this.orderService.getAllOrderByUser(request.user)
    }

    @UseGuards(ServiceAuthGuard)
    @Put()
    async validateOrder(@Req() request: RequestWithUser@Param('idReservation') idReservation: string ) {
        return this.orderService.validateOrder(idReservation)
    }
}