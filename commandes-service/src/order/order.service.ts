import { Injectable, NotFoundException } from "@nestjs/common";
import { UserDTO } from "./dto/user.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Order } from "src/entities/order.entity";
import { Repository } from "typeorm";
import { OrderDTO } from "./dto/order.dto";
import { Reservation } from "src/entities/reservation.entity";
import { RedisService, RedisPublication } from "@mistercoookie/nestjs-redis-pub-sub";


@Injectable()
export class OrderService {

    constructor(
        @InjectRepository(Order)
        private orderRepository: Repository<Order>,
        @InjectRepository(Reservation)
        private reservationRepository: Repository<Reservation>
    ) { }


    async sendOneOrder(user: UserDTO, data: OrderDTO, idReservation: string) {
        const reservation = await this.reservationRepository.findOne({
            where: {id: idReservation}
        })
        if(!reservation) {
            throw new NotFoundException('reservation not found')
        }
        const order = await this.orderRepository.create({
            ...data,
            user: user,
            reservation: reservation
        })

        RedisService.publish('ttjn-order', {
            id: order.id,
            price: order.price,
            content: order.content,
            user: order.user,
            reservation: order.reservation
        })

        await this.orderRepository.save(order)
        return order
    }

    async getAllOrderByUser(user: UserDTO) {
        return this.orderRepository.find({
            relations: ['user'],
            where: {
                user: {
                    id: user.id
                }
            },
        })
    }

    async validateOrder(idReservation: string) {
        const order = await this.orderRepository.findOne({
            where: {id: idReservation}
        })

        if(!order) {
            throw new NotFoundException('order not found')
        }

        order.validated = true
        await this.orderRepository.save(order)
        return order
    }
 }