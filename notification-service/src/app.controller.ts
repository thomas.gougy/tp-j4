import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import {RedisPublication, RedisService} from "@mistercoookie/nestjs-redis-pub-sub";
import {UserDTO} from "./dto/user.dto";
import {from} from "rxjs";
import {ReservationDTO} from "./dto/reservation";
import {OrderDto} from "./dto/order.dto";

const nodemailer = require("nodemailer");
var transport = nodemailer.createTransport({
    host: "sandbox.smtp.mailtrap.io",
    port: 2525,
    auth: {
        user: "f3b9af3efaa03e",
        pass: "4556254e0dbeda"
    }
});


@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {

    // inscription
    RedisService.subscribeChannel('ttjn-register',
      async (redisPublication: RedisPublication<UserDTO>) =>{
          console.log(redisPublication)
          let info = await transport.sendMail({
              from: '"hotel register" <register@hotel.com>', // sender address
              to: redisPublication.publishedData.email, // list of receivers
              subject: "Bienvenue parmis nous", // Subject line
              text: "bonjour " + redisPublication.publishedData.firstname + ", bienvenue parmis nos clients", // plain text body
              html: `<p>Bonjour ${redisPublication.publishedData.firstname} </p><p>Bienvenue parmis nos client</p>`, // html body
          });
      }
    )

    // login
    RedisService.subscribeChannel('ttjn-login',
        async (redisPublication: RedisPublication<UserDTO>) => {
          console.log(redisPublication)
            let info = await transport.sendMail({
                from: '"hotel login" <login@hotel.com>', // sender address
                to: redisPublication.publishedData.email, // list of receivers
                subject: "Vous venez de vous connecter", // Subject line
                text: "bonjour " + redisPublication.publishedData.firstname + ", nous venons d'enregistrer une connexion a votre compte", // plain text body
                html: `<p>Bonjour ${redisPublication.publishedData.firstname} </p><p>nous venons d'enregistrer une connexion a votre compte</p>`, // html body
            });

        })

      RedisService.subscribeChannel('ttjn-reservation',
          async (redisPublication: RedisPublication<ReservationDTO>) => {
              console.log(redisPublication)
              let info = await transport.sendMail({
                  from: '"hotel reservation" <reservation@hotel.com>', // sender address
                  to: redisPublication.publishedData.user.email, // list of receivers
                  subject: `Votre reservation numero ${redisPublication.publishedData.id}`, // Subject line
                  text: "bonjour " + redisPublication.publishedData.user.firstname + ", votre reservation a bien été prise en compte, nous vous conrfimerons celle ci sous 48H", // plain text body
                  html: `<p>Bonjour ${redisPublication.publishedData.user.firstname} </p><p>votre reservation a bien été prise en compte, nous vous conrfimerons celle ci sous 48H</p>`, // html body
              });

          })

      RedisService.subscribeChannel('ttjn-validate-reservation',
          async (redisPublication: RedisPublication<ReservationDTO>) => {
              console.log(redisPublication)
              let info = await transport.sendMail({
                  from: '"hotel reservation" <reservation@hotel.com>', // sender address
                  to: redisPublication.publishedData.user.email, // list of receivers
                  subject: `confirmation de votre reservation numero ${redisPublication.publishedData.id}`, // Subject line
                  text: "bonjour " + redisPublication.publishedData.user.firstname + ", nous vous confirmons que votre reservation a bien été validé par notre Hotel, nous serons ravi de vous acceuillir ", // plain text body
                  html: `<p>Bonjour ${redisPublication.publishedData.user.firstname} </p><p>nous vous confirmons que votre reservation a bien été validé par notre Hotel, nous serons ravi de vous acceuillir </p>`, // html body
              });

          })

      RedisService.subscribeChannel('ttjn-order',
          async (redisPublication: RedisPublication<OrderDto>) => {
              console.log(redisPublication)
              let info = await transport.sendMail({
                  from: '"hotel order" <order@hotel.com>', // sender address
                  to: redisPublication.publishedData.user.email, // list of receivers
                  subject: `confirmation de commande numero ${redisPublication.publishedData.id}`, // Subject line
                  text: "bonjour " + redisPublication.publishedData.user.firstname + ", nous vous confirmons que votre commande a bien été enregistré , nous vous enverrons un mail de confirmation une fois que celle ci sera validé  ", // plain text body
                  html: `<p>Bonjour ${redisPublication.publishedData.user.firstname} </p><p>nous vous confirmons que votre commande a bien été enregistré , nous vous enverrons un mail de confirmation une fois que celle ci sera validé </p>`, // html body
              });

          })

      RedisService.subscribeChannel('ttjn-confirm-order',
          async (redisPublication: RedisPublication<ReservationDTO>) => {
              console.log(redisPublication)
              let info = await transport.sendMail({
                  from: '"hotel order" <order@hotel.com>', // sender address
                  to: redisPublication.publishedData.user.email, // list of receivers
                  subject: `validation de votre commande numero ${redisPublication.publishedData.id}`, // Subject line
                  text: "bonjour " + redisPublication.publishedData.user.firstname + ", nous vous confirmons que votre commande a bien été validé.", // plain text body
                  html: `<p>Bonjour ${redisPublication.publishedData.user.firstname} </p><p>nous vous confirmons que votre commande a bien été validé. </p>`, // html body
              });

          })





  }


}
