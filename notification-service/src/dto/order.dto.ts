import {UserDTO} from "./user.dto";

export interface OrderDto {

    id: string

    price: number

    content: string

    validated: boolean;

    user: UserDTO


}