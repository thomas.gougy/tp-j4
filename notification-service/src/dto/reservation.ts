import {UserDTO} from "./user.dto";


export interface ReservationDTO{

    id: string
    validated: boolean
    room: string,
    user : UserDTO

}