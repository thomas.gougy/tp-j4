

export interface UserDTO {
    lastname: string
    firstname: string
    email: string
}

