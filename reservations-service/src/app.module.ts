import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { ReservationModule } from './reservation/reservation.module';
import {ServiceAuthStrategy} from "./guard/auth.guard";
import {ConfigModule} from "@nestjs/config";

@Module({
  imports: [DatabaseModule, ReservationModule],
  controllers: [],
  providers: [ServiceAuthStrategy],
})
export class AppModule {}
