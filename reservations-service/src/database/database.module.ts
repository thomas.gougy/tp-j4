import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {Reservation} from "./entities/reservation.entity";
import {User} from "./entities/user.entity";

@Module({
    imports: [TypeOrmModule.forRootAsync({
        imports: [ConfigModule.forRoot()],
        inject: [ConfigService],
        useFactory: (configService: ConfigService) => ({
            type: 'postgres',
            host: configService.get('BDD_HOST'),
            database: configService.get('BDD_DB'),
            port: configService.get('BDD_PORT'),
            username: configService.get('BDD_USER'),
            password: configService.get('BDD_PASSWORD'),
            entities: [Reservation, User],
            synchronize: true
        })
    })]
})
export class DatabaseModule {}
