import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./user.entity";

@Entity()
export class Reservation extends BaseEntity {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('boolean', { default: false })
    validated: boolean;

    @Column('varchar')
    room: string;

    @ManyToOne(() => User, (user) => user.reservations, { eager: true })
    user: User;
}
