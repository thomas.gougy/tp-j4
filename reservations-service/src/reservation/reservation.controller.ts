import {Body, Controller, Get, Param, Post, Put, Req, UseGuards} from '@nestjs/common';
import {ServiceAuthGuard} from "../guard/auth.guard";
import {User} from "../database/entities/user.entity";
import {Reservation} from "../database/entities/reservation.entity";
import {ReservationService} from "./reservation.service";
import {ReservationDto} from "./reservation.dto";

@Controller('reservation')
export class ReservationController {
    constructor(private reservationService: ReservationService) {}

    @UseGuards(ServiceAuthGuard)
    @Get()
    getReservation(@Req() request: { user: User }): Promise<Reservation[]> {
        return this.reservationService.getReservation(request.user);
    }

    @UseGuards(ServiceAuthGuard)
    @Get(':reservationId')
    getReservationValidated(@Req() request: { user: User }, @Param('reservationId') reservationId: string) {
        return this.reservationService.getOneReservation(request.user, reservationId);
    }

    @UseGuards(ServiceAuthGuard)
    @Post()
    reserveRoom(@Req() request: { user: User }, @Body() reservation: ReservationDto): Promise<Reservation> {
        return this.reservationService.reserveRoom(request.user, reservation);
    }

    @Put(':reservationId')
    validateReservation(@Param('reservationId') reservationId: string): Promise<Reservation> {
        return this.reservationService.validate(reservationId);
    }
}
