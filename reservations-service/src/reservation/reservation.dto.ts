import {IsNotEmpty, IsString} from "class-validator";

export class ReservationDto {
    @IsString()
    @IsNotEmpty()
    room: string;
}
