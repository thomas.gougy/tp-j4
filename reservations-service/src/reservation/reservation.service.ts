import {ConflictException, Injectable, NotFoundException, Res} from '@nestjs/common';
import {User} from "../database/entities/user.entity";
import {Reservation} from "../database/entities/reservation.entity";
import {RedisService} from "@mistercoookie/redis-pub-sub";
import {ReservationDto} from "./reservation.dto";

@Injectable()
export class ReservationService {
    getReservation(user: User): Promise<Reservation[]> {
        return Reservation.findBy({
            user: {
                id: user.id
            }
        });
    }

    async getOneReservation(user: User, reservationId: string): Promise<Reservation> {
        const reservation = await Reservation.findOneBy({ id: reservationId });

        if (reservation == null)
            throw new NotFoundException('Reservation not found');

        return reservation;
    }

    async validate(reservationId: string): Promise<Reservation> {
        let reservation: Reservation = await Reservation.findOneBy({ id: reservationId });

        if (reservation == null)
            throw new NotFoundException('Reservation not found');

        if (reservation.validated)
            throw new ConflictException('Reservation already validated');

        reservation.validated = true;
        reservation = await reservation.save();

        RedisService.publish('ttjn-validate-reservation', reservation);

        return reservation;
    }

    async reserveRoom(user: User, reservationData: ReservationDto): Promise<Reservation> {
        let reservation: Reservation = new Reservation()

        reservation.room = reservationData.room;
        reservation.user = user;

        reservation = await reservation.save()

        RedisService.publish('ttjn-reservation', reservation)

        return reservation
    }
}
