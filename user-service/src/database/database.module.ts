import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "./entities/user.entity";

@Module({
    // Les imports qui seront utilisé dans le modules
    imports: [
        // Initialisation du module typeorm
        TypeOrmModule.forRootAsync({
            // ConfigModule.forRoot()
            imports: [ConfigModule.forRoot()],
            inject: [ConfigService],
            useFactory: (configService: ConfigService) => ({
                type: 'postgres',
                host: configService.get('BDD_HOST'),
                port: configService.get('BDD_PORT'),
                username: configService.get('BDD_USER'),
                password: configService.get('BDD_PASSWORD'),
                database: configService.get('BDD_DATABASE'),
                entities: [User],
                synchronize: true
            })
        })
    ]
})
export class DatabaseModule {}
