import { Body, Controller, Get, Logger, Post, Req, UseGuards } from '@nestjs/common';
import { UserRegisterDTO } from './dtos/userRegister.dto';
import { UserLoginDTO } from './dtos/userLogin.dto';
import { UsersService } from './users.service';
import { JwtAuthenticationGuard, RequestWithUser } from 'src/security/jwt.guard';
import { RedisPublication, RedisService } from '@mistercoookie/nestjs-redis-pub-sub';

@Controller('users')
export class UsersController {
    private logger: Logger = new Logger('UsersController')

    constructor(
        private userService: UsersService,
    ) {

        RedisService.subscribeChannel('ttjn-validation', (redisPublication: RedisPublication<string>) => {
            this.onUserValidation(redisPublication)
        })
    }

    async onUserValidation(redisPublication: RedisPublication<string>) {
        const user = await this.userService.verifyToken(redisPublication.publishedData)
        RedisService.publish(redisPublication.answerChannel, user)
    }


    @Post('register')
    register(@Body() userRegisterDTO: UserRegisterDTO) {
        return this.userService.register(userRegisterDTO)
    }


    @Post('login')
    login(@Body() userLoginDTO: UserLoginDTO) {
        return this.userService.login(userLoginDTO)
    }


    @UseGuards(JwtAuthenticationGuard)
    @Get('me')
    me(@Req() request: RequestWithUser) {
        return request.user
    }
}
